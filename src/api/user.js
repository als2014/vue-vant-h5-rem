import qs from 'qs'
import request from '@/utils/request'
import { api } from '@/config'
// api
const { common_api } = api

// 登录
export function login(params) {
  return request({
    url: common_api + '/admin/login',
    method: 'post',
    data: qs.stringify(params)
  })
}

//获取列表信息
export function getList(params) {
  return request({
    url: common_api + '/admin/partmentuser',
    method: 'post',
    data: qs.stringify(params)
  })
}
