import { resolve } from "path";
import { login } from "../../api/user";

const state = {
  token:''
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
}

const actions = {
  //登陆
  Login({commit},userInfo){
    return Promise((resolve,reject)=>{
      login(userInfo.username,userInfo.password).then(response=>{
        const data=response.data;
        commit('SET_TOKEN',data.token);
        resolve();
      }).catch(error=>{
        reject(error)
      })

    })
  }
   
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
