import router from './router'

router.beforeEach((to, from, next) => {
  const title = to.meta && to.meta.title;
  if (title) {
    document.title = title;
  }
  next();
});

// router.beforeEach(async(to, from, next) => {
//   next()
// })
// router.afterEach(() => {})
