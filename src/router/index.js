import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
 const constantRoutes = [
  {
    path: '/',
    component: () => import('@/views/home/index'),
    meta: {
      keepAlive: false,
      title:'info'
    }
  },
  {
    path: '/goods',
    component: () => import('@/views/goods/index'),
    meta: {
      keepAlive: false,
      title:'info'
    }
  }
]


const createRouter = () =>
  new Router({
    // mode: '', history require service support
    base:  '/app/',
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  });


  
export default createRouter()
